# DEPRECATED!
> Sadly this version of Ubuntu's repositories stopped working as expected. Until a new ISO is created please download a fresh Ubuntu 20.04 and install the Ninja Way. https://shinobi.video/docs/start#content-the-ninja-way

# Ubuntu 19.10 Server ISO with Shinobi and CUDA

> This document will highlight the ISOs install process.

> ISO Download : https://cdn.shinobi.video/iso/ubuntu-19.10-server-amd64-shinobi-cuda-tensorflow-netinstall.iso

![alt text](https://gitlab.com/Shinobi-Systems/Shinobi-Installer/-/raw/dev/misc/ubuntu-images/splash2.jpg "ISO Splash")

# Install Process Highlights

1. Language for Installer
2. Select "Install Ubuntu Server"
3. Language for Installation of Ubuntu on the Server
4. Select your location
    - Hello from Canada.
5. Configure the Keyboard
    - If you don't know *just Press ENTER 3 times*.
        - No, English (US), English (US)
    - Do not Detect the layout. Press No.
    - Select English (US) if you don't know.
6. Set up Operating System User and Password
    - Make the "name" and "username" of the account *lowercase*.
7. Configure the clock
    - Press Yes, it should have automatically found the correct time zone.
8. You may be asked to *unmount partitions*. Say Yes.
9. Partition disks
    - If you don't know select *Guided - use entire disk*.
    - Then select the Disk to install to. If you have more than one disk be sure to select the correct one.
    - Confirm your selection. There is no undoing this choice.
10. Configure package manager : HTTP Proxy
    - If you don't know just press ENTER.
11. Software selection
    - The only one you'll really need is OpenSSH server.
12. Running preseed.
    - This stage will begin at 14% of "Finishing Installation"
    - Shinobi will be installed at this time
    - NVIDIA Drivers, CUDA Toolkit, CUDNN, and CUDNN Development Kit will be installed if an NVIDIA GPU is detected.
    - TensorFlow Object Detection plugin will be enabled and running by default.
        - If NVIDIA GPU is detected the installer will enable GPU computation.
        - If the precompiled version of TensorFlow is not compatible with your hardware then it will automatically suspend itself and disconnect from the main process.
13. After Reboot login to the terminal with the account you created mentioned in \#6.
    - Run `ifconfig | grep "inet "` to get the IP Address of the machine.
        - It is the value after `inet`.
    - Default Shinobi port is 8080.
14. Open the address in your web browser in this format `http://YOUR_SHINOBI_MACHINE_IP:8080/super`. Login as the Superuser and create an Account.
    - Default Superuser Username : admin@shinobi.video
    - Default Superuser Password : admin
    - You only need to set the Email and Password for a New Account.
        - No emails will be sent to this address unless you ask for it to occur. This information is not sent to Shinobi Systems.
15. Login with the new account at `http://YOUR_SHINOBI_MACHINE_IP:8080` to start adding cameras.


# Shinobi Terminal Commands

Your Shinobi processes can be accessed via `pm2`. You will need to prefix all commands with `sudo` if you are not running commands as root. To become root run `sudo su`.

- `pm2 list` : Shows all the currently running processes.
    - `camera.js` : The main Shinobi Daemon.
    - `cron.js` : Supplemental purging processes based on time intervals.
    - `shinobi-tensorflow.js` : TensorFlow Object Detection plugin for Shinobi.
- `pm2 logs --lines 100` : This will show the last 100 lines of the Shinobi system logs and begin streaming new logs.
    - **red text** : Critical errors.
    - **green text** : General information. Can be "caught" errors as well.
- `pm2 restart all` : Restart all processes running in `pm2 list`
- `pm2 flush` : Clear system logs.
- `pm2 flush && pm2 restart all && pm2 logs` : This one-liner will flush old logs, restart Shinobi, and display the startup logs as a stream.
- `cd /home/Shinobi && sh UPDATE.sh` : Will update Shinobi. Manual restart of Shinobi required.


# Writing the ISO to a USB Stick

- Windows : https://rufus.ie/
    - Fairly straight forward. Download Rufus, Open it, Select the ISO, Select your USB stick and hit START.
    - Ensure Rufus options are set
        - Partition Scheme : MBR
        - Target System : BIOS or UEFI
- MacOS : https://ubuntu.com/tutorials/create-a-usb-stick-on-macos#1-overview
- Linux : Based on your flavor of Linux your writing method may vary.


# Additional Links

- What to do after installation : https://shinobi.video/docs/configure
- Adding an ONVIF Amcrest 4K (8MP) Camera and configuring Motion Detection : https://www.youtube.com/watch?v=v-iwKuJ-eDs
- Adding Reolink Cameras to Shinobi : https://www.youtube.com/watch?v=nwzpSUy6Ias
