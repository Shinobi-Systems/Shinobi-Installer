
> Tested on Ubuntu 19.10 Server AMD64

> http://old-releases.ubuntu.com/releases/19.10/

> http://old-releases.ubuntu.com/releases/19.10/ubuntu-19.10-server-amd64.iso

# Mount an ISO

Reference : https://askubuntu.com/questions/164227/how-to-mount-an-iso-file

1. Make directory.
```
sudo mkdir /media/iso
```

2. Mount ISO to Directory.
```
sudo mount -o loop ./ubuntu-19.10-server-amd64.iso /media/iso
```

# Extract Mounted ISO

```
cp -r /media/iso isoFile/
```

# Add Shinobi to Preseed

At the bottom of the `.seed` file add the following. `.seed` files are found in the `isoFile/preseed` folder. `isoFile` is the folder where your ISO was extracted to.

```
d-i preseed/late_command string in-target wget -P /tmp/ https://gitlab.com/Shinobi-Systems/Shinobi-Installer/-/raw/dev/shinobi-ubuntu-1910-postinstall.sh; in-target chmod +x /tmp/shinobi-ubuntu-1910-postinstall.sh; in-target /tmp/shinobi-ubuntu-1910-postinstall.sh
```

# Build ISO from Folder

```
mkisofs -o ubuntu-19.10-server-amd64-shinobi-cuda-tensorflow-netinstall.iso -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -J -R -V "ShinobiUbuntu19" isoFile/
```

# Unmount ISO

```
sudo umount /media/iso
```

# Getting CDROM not found error?

Attempt the following :

- Rewrite ISO to USB
- Change USB Ports
- Change USB Sticks
- Update Rufus (Windows ISO Writer)
