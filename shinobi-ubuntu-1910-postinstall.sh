#! /bin/sh

installLocation="/home"
cd $installLocation

sudo apt update -y
sudo apt install git openssh-server -y

echo "Getting the Development Branch"
theBranch='dev'
gitURL="https://gitlab.com/Shinobi-Systems/Shinobi"
sudo git clone $gitURL.git -b $theBranch Shinobi
cd Shinobi
gitVersionNumber=$(git rev-parse HEAD)
theDateRightNow=$(date)

echo "-----------------------------------"
echo $gitVersionNumber
echo $theDateRightNow
echo "-----------------------------------"
sudo chmod +x INSTALL/ubuntu-touchless-iso.sh
sudo INSTALL/ubuntu-touchless-iso.sh


if lspci -vnn | grep -q NVIDIA; then
    tfjsBuildVal='gpu'
    cd /home/Shinobi/INSTALL
    wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/cuda-repo-ubuntu1804_10.0.130-1_amd64.deb
    sudo dpkg -i --force-overwrite cuda-repo-ubuntu1804_10.0.130-1_amd64.deb
    sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub

    sudo apt-get update -y

    sudo apt-get -o Dpkg::Options::="--force-overwrite" install cuda-toolkit-10-0 -y --no-install-recommends
    sudo apt-get -o Dpkg::Options::="--force-overwrite" install --fix-broken -y
    sudo apt install nvidia-utils-440 nvidia-headless-440 -y

    # Install CUDA DNN
    wget https://cdn.shinobi.video/installers/libcudnn7_7.6.5.32-1+cuda10.0_amd64.deb -O cuda-dnn.deb
    sudo dpkg -i cuda-dnn.deb
    wget https://cdn.shinobi.video/installers/libcudnn7-dev_7.6.5.32-1+cuda10.0_amd64.deb -O cuda-dnn-dev.deb
    sudo dpkg -i cuda-dnn-dev.deb
    echo "-- Cleaning Up --"
    # Cleanup
    sudo rm cuda-dnn.deb
    sudo rm cuda-dnn-dev.deb
    cd /home/Shinobi/plugins/tensorflow
    sudo npm install yarn -g --unsafe-perm --force
    sudo npm install @tensorflow/tfjs-node-gpu@1.7.3 --unsafe-perm
else
    tfjsBuildVal='cpu'
    cd /home/Shinobi/plugins/tensorflow
    sudo npm install yarn -g --unsafe-perm --force
    sudo npm install @tensorflow/tfjs-node@1.7.3 --unsafe-perm
fi
sudo npm install --unsafe-perm
sudo npm audit fix --force
sudo cp conf.sample.json conf.json
cd /home/Shinobi
sudo node tools/modifyConfigurationForPlugin.js tensorflow key=$(head -c 64 < /dev/urandom | sha256sum | awk '{print substr($1,1,60)}') tfjsBuild=$tfjsBuildVal
cd /home/Shinobi/plugins/tensorflow
echo "TF_FORCE_GPU_ALLOW_GROWTH=true" > ".env"
echo "#CUDA_VISIBLE_DEVICES=0,2" >> ".env"
sudo pm2 start shinobi-tensorflow.js
sudo pm2 save
